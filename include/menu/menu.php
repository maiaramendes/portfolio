
<!-- Offcanvas Menu Section -->
	<div class="menu-wrapper">
		<div class="menu-switch">
			<!-- <i class="ti-menu"></i> -->
		</div>

		<nav class="main-menu">
			<ul>
				<li><a href="index.html" class="active"><i class="fas fa-home home-change"></i></a></li>
				<li><a href="about.html"><i class="fas fa-user about-change"></i></a></li>
				<li><a href="gallery.html"><i class="fas fa-desktop portfolio-change"></i></a></li>				
				<li><a href="contact.html"><i class="fas fa-envelope contact-change"></i></a></li>
			</ul>
		</nav>

		<div class="menu-social-warp">
			<div class="menu-social">								
				<a href="#"><i class="ti-linkedin"></i></a>				
			</div>
		</div>
	</div>
	<div class="side-menu-wrapper">
		<div class="sm-header">
			<div class="menu-close">
				<i class="ti-arrow-left"></i>
			</div>
			<a href="index.html" class="site-logo">
				<img src="img/logo.png" alt="">
			</a>
		</div>
		
		<div class="sm-footer">
			<div class="sm-socail">
				<a href="#"><i class="ti-facebook"></i></a>
				<a href="#"><i class="ti-twitter-alt"></i></a>
				<a href="#"><i class="ti-linkedin"></i></a>
				<a href="#"><i class="ti-instagram"></i></a>
			</div>
			<div class="copyright-text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  
		</div>
	</div>
	<!-- Offcanvas Menu Section end -->

