<!DOCTYPE html>
<html lang="zxx">
		<?php

		include 'include/head/head.php';

		?>
<body>

	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<?php include 'include/menu/menu.php'; ?>

	<!-- Hero Section end -->
	<section class="hero-section">
		<div class="pana-accordion" id="accordion">
    		<div class="pana-accordion-wrap">
				<div class="pana-accordion-item set-bg" data-setbg="img/hero/5.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="pana-accordion-item set-bg" data-setbg="img/hero/1.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="pana-accordion-item set-bg" data-setbg="img/hero/2.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="pana-accordion-item set-bg" data-setbg="img/hero/3.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="pana-accordion-item set-bg" data-setbg="img/hero/4.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
	    	</div>
		</div>
		<div class="hero-slider-warp">
			<div class="hero-slider owl-carousel">
				<div class="hero-item set-bg" data-setbg="img/hero/5.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="hero-item set-bg" data-setbg="img/hero/1.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="hero-item set-bg" data-setbg="img/hero/2.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="hero-item set-bg" data-setbg="img/hero/3.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
				<div class="hero-item set-bg" data-setbg="img/hero/4.jpg">
					<div class="pa-text">
						<div class="pa-tag">people</div>
						<h2>69 Flavio Burg Suite</h2>
						<div class="pa-author">
							<img src="img/Hero/author.jpg" alt="">
							<h4>Arthur Rose</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero Section end -->

	

	</body>
<style>
.menu-social-warp {
	background-color: #151515;
}


</style>
</html>
